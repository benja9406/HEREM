#include <TimerOne.h> //Libreria TimerOne utilizada para realizar las interrupciones
#include<Servo.h>     //Libreria para utilizar servomotores.

int valor_analogico; //Variable que guarda la señal analogica que envía el musculo
boolean estado = true; //Variable booleana para ver el estado de la mano

Servo servo1;
Servo servo2;
Servo servo3;

void setup() {
  Serial.begin(115200);
  Timer1.initialize(1000000);
  Timer1.attachInterrupt(interrupcion);

  servo1.attach(8);
  servo2.attach(9);
  servo3.attach(10);
  interrupts();
}

void interrupcion() {
  Serial.println("--------------1 Segundo--------------");
}

void loop() {
  valor_analogico = analogRead(A0);
  Serial.println(valor_analogico);
  if (valor_analogico >= 800) {opcion(estado);}
}

void opcion(boolean auxbool) {
  Serial.println("Entra a metodo");
  if(auxbool == true){
    abrirmano();
    }
    else {cerrarmano();}
  delay(250);
}

void abrirmano(){
  /*
   * Este metodo solo se ejecutará si la varibale boolena es verdadera
   *eso nos indica que la mano ha estado cerrada, y al cambiar la variable
   *estado a true, la mano debe estar abierta.
   *
  */
  Serial.println("Mano abierta");
  servo1.write(0);
  servo2.write(0);
  servo3.write(0);
  estado = !estado;
  delay(100);
  }

void cerrarmano(){
  /*
   * Este metodo solo se ejecutará si la variable booleana es falsa
   * anteriormente debío estar en verdadera, al ser falsa, nos indica 
   * que la mano debe estar cerrada.
  */
  Serial.println("manocerrada");
  servo1.write(100);
  servo2.write(100);
  servo3.write(90);
  estado = !estado;
  delay(100);
  }
